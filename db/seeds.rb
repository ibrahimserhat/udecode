user = User.create(email: 'admin@local.app', password: '123123')

30.times do
  Course.create!([{
                      title: Faker::Educator.course_name,
                      description: Faker::TvShows::GameOfThrones.quote,
                      user_id: user.id
                  }])
end
