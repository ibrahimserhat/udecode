class Course < ApplicationRecord
  validates :title, presence: true, length: { minimum: 5 }
  validates :description, presence: true, length: { minimum: 15 }

  has_rich_text :description

  belongs_to :user
end
