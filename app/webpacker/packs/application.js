require("@rails/ujs").start()
require("@rails/activestorage").start()
require("channels")
require("trix")
require("@rails/actiontext")

// CSS
import 'scss/site'

// JS
import 'js/site'

// Images
const images = require.context('../images', true)
const imagePath = (name) => images(name, true)
